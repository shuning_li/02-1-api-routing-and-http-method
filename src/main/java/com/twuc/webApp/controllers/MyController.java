package com.twuc.webApp.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MyController {
    @GetMapping("/users/{id}/books")
    public String getUserId(@PathVariable int id) {
        return "The book for user " + id;
    }

    @GetMapping("/segments/good")
    public String getSegmentsExactly() {
        return "match exactly";
    }

    @GetMapping("/segments/{segmentName}")
    public String getSegments(@PathVariable String segmentName) {
        return "match not exactly";
    }

    @GetMapping("/single/?")
    public String getParamsUsingSingleMatchSymbol() {
        return "match";
    }

    @GetMapping("/wildcards/*")
    public String getStringWhenWildCardBehind() {
        return "1";
    }

    @GetMapping("/wildcards/*/abc")
    public String getStringWhenWildCardInMiddle() {
        return "1";
    }

    @GetMapping("/wildcard/before/*/after")
    public String getStringWhenWildcardsNotMatch() {
        return "1";
    }

    @GetMapping("/wildcard/*.abc")
    public String getStringUsingPrefixWildcard() {
        return "1";
    }

    @GetMapping("/wildcard/anything.*")
    public String getStringUsingSuffixWildcard() {
        return "1";
    }

    @GetMapping("/wildcard/**/abc")
    public String getStringWhenMultiWildcardInMiddle() {
        return "1";
    }

    @GetMapping("/regex/{id:\\d+}")
    public String getStringWhenUsingRegex(@PathVariable int id) {
        return "" + id;
    }

    @GetMapping("/student")
    public String getStudentName(@RequestParam(name = "name") String name, @RequestParam(name = "age", required = false) Integer age) {
        return name;
    }

}
