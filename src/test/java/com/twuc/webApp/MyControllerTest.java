package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MyControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_get_user_id() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(content().string("The book for user 2"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(content().string("The book for user 23"))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_404_when_pass_uppcase_while_it_requireds_lowercas() throws Exception {
        mockMvc.perform(get("/api/users/2/BOOKS"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_match_exact() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(content().string("match exactly"));
    }

    @Test
    void should_be_ok_when_passing_one_alphabet_to__single_match_symbol() throws Exception {
        mockMvc.perform(get("/api/single/a"))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_404_when_passing_more_than_one_alphabet_to_single_match_symbol() throws Exception {
        mockMvc.perform(get("/api/single/abc"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_get_404_when_passing_no_alphabet_to_single_match_symbol() throws Exception {
        mockMvc.perform(get("/api/single"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_get_200_when_pass_path_variable_to_wildcard() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything"))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_200_when_pass_path_variable_to_wildcard_which_it_is_in_the_middle() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything/abc"))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_404_when_not_matching_in_wildcard() throws Exception {
        mockMvc.perform(get("/api/before/after"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_get_200_when_prefix_match() throws Exception {
        mockMvc.perform(get("/api/wildcard/anything.abc"))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_200_when_suffix_match() throws Exception {
        mockMvc.perform(get("/api/wildcard/anything.do"))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_200_when_putting_multi_wildcard_in_middle() throws Exception {
        mockMvc.perform(get("/api/wildcard/any/abc"))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_200_when_path_contain_regex() throws Exception {
        mockMvc.perform(get("/api/regex/123"))
                .andExpect(status().isOk())
                .andExpect(content().string("123"));
    }

    @Test
    void should_get_string_when_using_query() throws Exception {
        mockMvc.perform(get("/api/student?name=shuning"))
                .andExpect(status().isOk())
                .andExpect(content().string("shuning"));
    }

    @Test
    void should_get_4xx_when_using_query_but_not_pass_params() throws Exception {
        mockMvc.perform(get("/api/student"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_get_4xx_when_set_specific_query_which_is_not_accessible() throws Exception {
        mockMvc.perform(get("/api/student?age=12"))
                .andExpect(status().isBadRequest());
    }
}
